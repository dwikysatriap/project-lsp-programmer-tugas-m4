<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_tokobuku');
	}

	public function index()
	{
		$dataBuku = $this->M_tokobuku->getDataBuku();
		/*echo "<pre>";
		print_r($dataBuku);
		echo "<pre>";*/
		$data = array('data_buku' => $dataBuku);
		$this->load->view('home', $data);
	}

	public function tambahBuku()
	{
		$this->load->view('tambah_buku');
	}

	public function editBuku($id)
	{
		$dataBukuDetail = $this->M_tokobuku->getDataBukuDetail($id);

		$DataBuku = array('data_buku' => $dataBukuDetail);
		$this->load->view('edit_buku', $DataBuku);
	}

	public function p_tambahData()
	{
		$id_buku = $this->input->post('id_buku');
		$kategori_buku = $this->input->post('kategori');
		$judul_buku = $this->input->post('judul');
		$penerbit = $this->input->post('penerbit');
		$harga = $this->input->post('harga');

		$insertData = array(
			'id_buku' => $id_buku,
			'kategori_buku' => $kategori_buku,
			'judul_buku' => $judul_buku,
			'penerbit' => $penerbit,
			'harga' => $harga
		);

		$this->M_tokobuku->insertDataBuku($insertData);
		redirect(base_url('Welcome'));
	}

	public function p_editBuku()
	{
		$id_buku = $this->input->post('id_buku');
		$kategori_buku = $this->input->post('kategori');
		$judul_buku = $this->input->post('judul');
		$penerbit = $this->input->post('penerbit');
		$harga = $this->input->post('harga');

		$updateData = array(
			'kategori_buku' => $kategori_buku,
			'judul_buku' => $judul_buku,
			'penerbit' => $penerbit,
			'harga' => $harga
		);

		$this->M_tokobuku->editDataBuku($updateData, $id_buku);
		redirect(base_url('Welcome'));
	}

	public function deleteBuku($id_buku)
	{
		$this->M_tokobuku->deleteDataBuku($id_buku);
		redirect(base_url('Welcome'));
	}
}
