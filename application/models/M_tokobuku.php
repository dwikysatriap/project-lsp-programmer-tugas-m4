<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tokobuku extends CI_Model
{

    public function getDataBuku()
    {
        $this->db->select('*');
        $this->db->from('tb_buku');
        $query = $this->db->get();
        return $query->result();
    }

    public function insertDataBuku($data)
    {
        $this->db->insert('tb_buku', $data);
    }

    public function editDataBuku($data, $id)
    {
        $this->db->where('id_buku', $id);
        $this->db->update('tb_buku', $data);
    }

    public function getDataBukuDetail($id)
    {
        $this->db->where('id_buku', $id);
        $query = $this->db->get('tb_buku');
        return $query->row();
    }

    public function deleteDataBuku($id)
    {
        $this->db->where('id_buku', $id);
        $this->db->delete('tb_buku');
    }
}
