<html>

<head>
    <title>Ubah Data Buku</title>
</head>

<body>
    <h3>Form Ubah Data Buku</h3>
    <table>
        <form action="<?php echo base_url('Welcome/p_editBuku'); ?>" method="POST">
            <tr>
                <td>ID Buku</td>
                <td>:</td>
                <td>
                    <input type="text" value="<?php echo $data_buku->id_buku; ?>" name="id_buku" disabled>
                    <!--menampilkan data-->
                    <input type="hidden" name="id_buku" value="<?php echo $data_buku->id_buku; ?>">
                    <!--menampung data-->
                </td>
            </tr>
            <tr>
                <td>Kategori Buku</td>
                <td>:</td>
                <td><input type="text" name="kategori" value="<?php echo $data_buku->kategori_buku; ?>"></td>
            </tr>
            <tr>
                <td>Judul Buku</td>
                <td>:</td>
                <td><input type="text" name="judul" value="<?php echo $data_buku->judul_buku; ?>"></td>
            </tr>
            <tr>
                <td>Penerbit</td>
                <td>:</td>
                <td><input type="text" name="penerbit" value="<?php echo $data_buku->penerbit; ?>"></td>
            </tr>
            <tr>
                <td>Harga</td>
                <td>:</td>
                <td><input type="number" name="harga" value="<?php echo $data_buku->harga; ?>"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <center><input type="submit" value="SIMPAN"></center>
                </td>
            </tr>
        </form>
    </table>
</body>

</html>