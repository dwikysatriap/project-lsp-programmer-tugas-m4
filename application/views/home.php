<html>

<head>
    <title>dsp81099</title>
</head>

<body>
    <h1>DSP_81099 BOOK STORE</h1>
    <h3>STOK BUKU</h3>

    <button onclick="document.location.href = '<?php echo base_url('Welcome/tambahBuku'); ?>' ">Tambah Data</button>
    <table>
        <tr>
            <td><b>No</b></td>
            <td><b>ID Buku</b></td>
            <td><b>Kategori Buku</b></td>
            <td><b>Judul Buku</b></td>
            <td><b>Penerbit</b></td>
            <td><b>Harga</b></td>
            <td><b>Opsi</b></td>
        </tr>
        <?php
        $no = 1;
        foreach ($data_buku as $row) {

        ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $row->id_buku; ?></td>
                <td><?php echo $row->kategori_buku; ?></td>
                <td><?php echo $row->judul_buku; ?></td>
                <td><?php echo $row->penerbit; ?></td>
                <td><?php echo $row->harga; ?></td>
                <td><a href="<?php echo base_url('Welcome/editBuku/') . $row->id_buku; ?> ">Edit</a> |
                    <a href="<?php echo base_url('Welcome/deleteBuku/') . $row->id_buku; ?>">Hapus</a></td>
            </tr>

        <?php
        } ?>
    </table>
</body>

</html>