<html>

<head>
    <title>Tambah Data Buku</title>
</head>

<body>
    <h3>Form Input Data Buku</h3>
    <table>
        <form action="<?php echo base_url('Welcome/p_tambahData') ?>" method="POST">
            <tr>
                <td>ID Buku</td>
                <td>:</td>
                <td><input type="text" name="id_buku" disabled></td>
            </tr>
            <tr>
                <td>Kategori Buku</td>
                <td>:</td>
                <td><input type="text" name="kategori" required></td>
            </tr>
            <tr>
                <td>Judul Buku</td>
                <td>:</td>
                <td><input type="text" name="judul" required></td>
            </tr>
            <tr>
                <td>Penerbit</td>
                <td>:</td>
                <td><input type="text" name="penerbit" required></td>
            </tr>
            <tr>
                <td>Harga</td>
                <td>:</td>
                <td><input type="number" name="harga" required></td>
            </tr>
            <tr>
                <td colspan="3">
                    <center><input type="submit" value="SIMPAN"></center>
                </td>
            </tr>
        </form>
    </table>
</body>

</html>